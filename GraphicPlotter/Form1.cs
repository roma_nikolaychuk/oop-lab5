﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicPlotter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            one();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            one();
        }

        private void one()
        {
            Graphics g = this.CreateGraphics(); //задаємо область рисування
            g.Clear(Color.White);              //очищаємо область (заливаємо білим кольором)
            Pen a = new Pen(Color.Blue, 1);     //перо для рисування осей
            Pen b = new Pen(Color.Green, 2);    //перо для рисування графіка
            Font drawFont = new Font("Arial", 12); //шрифт для підпису осей
            Font signatureFont = new Font("Arial", 7); //шрифт для підпису поділок осі
            SolidBrush drawBrush = new SolidBrush(Color.Blue);//колір шрифта
            StringFormat drawFormat = new StringFormat();     //формат букв (для підпису)
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;//напрямок тексту осі
            int sizeWidth = Form1.ActiveForm.Width;   //ширина вікна програми
            int sizeHeight = Form1.ActiveForm.Height; //висота вікна програми
            //x,y – координати цента (точка (0,0))
            Point center = new Point(((int)(sizeWidth / 2) - 8), (int)((sizeHeight / 2) - 19));

            ////////   будуємо осі  ////////
            g.DrawLine(a, 10, center.Y, center.X, center.Y);                //вісь X+
            g.DrawLine(a, center.X, center.Y, 2 * center.X - 10, center.Y); //вісь X‐
            g.DrawLine(a, center.X, 10, center.X, center.Y);                //вісь Y+
            g.DrawLine(a, center.X, center.Y, center.X, 2 * center.Y - 10); //вісь Y‐

            g.DrawString("X", drawFont, drawBrush, new PointF(2 * center.X - 5, center.Y + 10), drawFormat);  //підписали X
            g.DrawString("Y", drawFont, drawBrush, new PointF(center.X + 30, 5), drawFormat);              //підписали Y
            g.DrawString("0", signatureFont, drawBrush, new PointF(center.X, center.Y), drawFormat);         //підписали 0
            ////////    рисуємо стрілочки    ////////
            g.DrawLine(a, center.X, 10, center.X + 5, 20);
            g.DrawLine(a, center.X, 10, center.X - 5, 20);
            g.DrawLine(a, sizeWidth - 25, center.Y, sizeWidth - 30, center.Y + 5);
            g.DrawLine(a, sizeWidth - 25, center.Y, sizeWidth - 30, center.Y - 5);
            int stepForAxes = 25;   //віддаль між поділками на осях
            int lenghtShtrih = 3;   //половина довжини штришка поділки
            int maxValueForAcesX = 20;  //максимальне значення по осі X
            int maxValueForAcesY = 10;  //максимальне значення по осі Y
            float oneDelenieX = (float)maxValueForAcesX / ((float)center.X / (float)stepForAxes);  //то, чим підписуємо поділки X
            float oneDelenieY = (float)maxValueForAcesY / ((float)center.Y / (float)stepForAxes);  //то, чим підписуємо поділки Y

            ////////    рисуємо штрихи та координати на прямій Х   ////////
            for (int i = center.X, j = center.X, k = 1; i < 2 * center.X - 30; j -= stepForAxes, i += stepForAxes, k++)
            {
                //рисуємо штрихи вздовж осі X праворуч від центра 
                g.DrawLine(a, i, center.Y - lenghtShtrih, i, center.Y + lenghtShtrih);
                //рисуємо штрихи вздовж осі X ліворуч від центра 
                g.DrawLine(a, j, center.Y - lenghtShtrih, j, center.Y + lenghtShtrih);
                if (i < 2 * center.X - 55)
                {
                    g.DrawString((k * oneDelenieX).ToString("0.0"), signatureFont, drawBrush, new PointF(i + stepForAxes + 9, center.Y + 6), drawFormat);  //підписуємо поділки +
                    g.DrawString(((k * oneDelenieX).ToString("0.0").ToString() + "‐"), signatureFont, drawBrush, new PointF(j - stepForAxes + 9, center.Y + 6), drawFormat); // ‐
                }
            }

            ////////    малюємо штрихи та координати на прямій Y   ////////
            for (int i = center.Y, j = center.Y, k = 1; i < 2 * center.Y - 30; j -= stepForAxes, i += stepForAxes, k++)
            {
                //рисуємо штрихи вздовж осі Y догори від центра 
                g.DrawLine(a, center.X - lenghtShtrih, i, center.X + lenghtShtrih, i);
                //рисуємо штрихи вздовж осі X донизу від центра 
                g.DrawLine(a, center.X - lenghtShtrih, j, center.X + lenghtShtrih, j);
                if (i < 2 * center.Y - 55)
                {
                    g.DrawString((k * oneDelenieY).ToString("0.0"), signatureFont, drawBrush, new PointF(center.X + 22, j + stepForAxes + 9), drawFormat);  //підписуємо поділки +
                    g.DrawString(((k * oneDelenieY).ToString("0.0").ToString() + "‐"), signatureFont, drawBrush, new PointF(center.X + 22, i - stepForAxes + 9), drawFormat);  // ‐
                }
            }

            int numOfPoint = 200;  //кількість точок для обчислення значення функції 
            float[] first = new float[numOfPoint];  //точки для обчислення
            for (int i = 0; i < numOfPoint; i++)
            {
                first[i] = (float)maxValueForAcesX / (float)numOfPoint * (i + 1) - (float)(maxValueForAcesX / 2);  //проміжок від ‐2 до 2
            }
            float[] second = new float[numOfPoint];  //значення функції в точках
            ////////   Обчислення значень функції  ////////
            for (int i = 0; i < numOfPoint; i++)
            {
                second[i] = first[i] * (float)Math.Sin(5 * first[i]);
            }
            Point[] pointOne = new Point[numOfPoint];
            float tempX = 1 / oneDelenieX * stepForAxes;  //розраховуємо нові координати
            float tempY = 1 / oneDelenieY * stepForAxes;
            for (int i = 0; i < numOfPoint; i++)
            {
                pointOne[i].X = center.X + (int)(first[i] * tempX); //перехід до нових координат
                pointOne[i].Y = center.Y - (int)(second[i] * tempY);
            }
            ////////   рисуємо графік   ////////
            g.DrawCurve(b, pointOne);  //сплайном
            g.DrawLines(b, pointOne);  //ламаною лінією

    }
    }

}